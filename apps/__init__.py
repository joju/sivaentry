from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('apps.config')
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

from apps import views, models

login_manager.login_view = "login"

@login_manager.user_loader
def load_user(user_id):
    return models.Admin.query.get(user_id)
