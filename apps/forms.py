from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, HiddenField, SelectField
from wtforms.validators import DataRequired

from apps.models import EmployeeType


class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class EmployeeHourForm(Form):
    employee_id = StringField('ID', validators=[DataRequired()])
    entry_date = StringField('Entry Date', validators=[DataRequired()])  # , placeholder='DD/YY/MMMM'
    start_time = StringField('Start Time', validators=[DataRequired()])  # , placeholder='HH:MM'
    end_time = StringField('End Time', validators=[DataRequired()])  # , placeholder='HH:MM'


class EditEmployeeHourForm(Form):
    employee_id = HiddenField('ID', validators=[DataRequired()])
    entry_date = StringField('Entry Date', validators=[DataRequired()])  # , placeholder='DD/YY/MMMM'
    start_time = StringField('Start Time', validators=[DataRequired()])  # , placeholder='HH:MM'
    end_time = StringField('End Time', validators=[DataRequired()])  # , placeholder='HH:MM'


class EmployeeTypeForm(Form):
    slug = StringField('Slug', validators=[DataRequired()])
    rate = StringField('Rate', validators=[DataRequired()])
    description = StringField('Description')


class EmployeeHourDeleteForm(Form):
    object_id = HiddenField('ID', validators=[DataRequired()])


class SalarySearchForm(Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.emp_type.choices = [(emp_type.id, emp_type.slug) for emp_type in EmployeeType.query.all()]

    emp_type = SelectField(u'Employee Type')
    month = SelectField(u'Employee Type', choices=[('None', 'Select')] + [(x, x) for x in range(1, 13)])
    year = SelectField(u'Employee Type', choices=[('None', 'Select')] + [(x, x) for x in range(2016, 2020)])
