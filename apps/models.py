from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import datetime

from apps import db


class Admin(UserMixin, db.Model):
    __tablename__ = 'admin_user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    name = db.Column(db.String(120), index=True)
    password_hash = db.Column(db.String(120), index=True)

    def __init__(self, username, password, *args, **kwargs):
        self.username = username
        self.set_password(password)
        super().__init__(*args, **kwargs)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)


class Employee(db.Model):
    __tablename__ = 'employee'

    id = db.Column(db.Integer, primary_key=True)
    employee_type_id = db.Column(db.Integer, db.ForeignKey('employee_type.id'))
    hours = db.relationship('EmployeeHours', backref='employee', lazy='dynamic')
    salary = db.relationship('EmployeeSalary', backref='employee', lazy='dynamic')
    emp_id = db.Column(db.String(20), index=True)

    def __repr__(self):
        return '<Employee %r>' % (self.name)


class EmployeeType(db.Model):
    __tablename__ = 'employee_type'

    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(120), index=True)
    rate = db.Column(db.Float)
    description = db.Column(db.String(120), index=True)


class EmployeeHours(db.Model):
    __tablename__ = 'employee_hours'

    id = db.Column(db.Integer, primary_key=True)
    entry_date = db.Column(db.Date)
    start = db.Column(db.String)
    end = db.Column(db.String)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    hours = db.Column(db.Float)

    def __repr__(self):
        return '<Employee Hour %r - %r>' % (self.id, self.entry_date)


class EmployeeSalary(db.Model):
    __tablename__ = 'employee_salary'

    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    salary = db.Column(db.Float)
    month = db.Column(db.Integer)
    year = db.Column(db.Integer)
