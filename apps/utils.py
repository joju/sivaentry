import re


def split_by_emp_type(emp_id):
    return filter(None, re.split(r'(\d+)', emp_id))
