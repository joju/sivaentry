from flask import render_template, redirect, flash, g
from flask_login import login_required, current_user, login_user
from datetime import datetime

from apps import app
from apps.forms import (
    LoginForm, EmployeeHourForm, EmployeeTypeForm, EmployeeHourDeleteForm,
    SalarySearchForm
)
from apps.models import Admin, Employee, EmployeeHours, EmployeeType, db, EmployeeSalary
from apps.utils import split_by_emp_type


@app.route('/')
@app.route('/index')
def index():
    return render_template('welcome.html')


@app.route('/employees')
@login_required
def employees():
    employees = Employee.query.all()
    return render_template('index.html', employees=employees)


@app.route('/add_time', methods=['GET', 'POST'])
@login_required
def add_employee_hours():
    form = EmployeeHourForm()
    if form.validate_on_submit():
        emp_id = form.data['employee_id']
        employee = Employee.query.filter_by(emp_id=emp_id).first()
        emp_type = [data for data in split_by_emp_type(emp_id)][0]
        employee_type = EmployeeType.query.filter_by(slug=emp_type).first()
        if not employee:
            employee = Employee(emp_id=emp_id, employee_type_id=employee_type.id)
            db.session.add(employee)
        entry_date = form.data['entry_date']
        start = form.data['start_time']
        end = form.data['end_time']
        start_time = '{} {}'.format(entry_date, start)
        end_time = '{} {}'.format(entry_date, end)
        entry_date = datetime.strptime(entry_date, '%d/%m/%Y')
        start_time = datetime.strptime(start_time, '%d/%m/%Y %H:%M')
        end_time = datetime.strptime(end_time, '%d/%m/%Y %H:%M')
        hours = ((end_time - start_time).seconds / 3600)
        emp_hours = EmployeeHours(entry_date=entry_date, start=start, end=end, employee=employee, hours=hours)
        db.session.add(emp_hours)
        emp_sal = EmployeeSalary.query.filter_by(employee_id=employee.id, month=entry_date.month, year=entry_date.year).first()
        if not emp_sal:
            emp_sal = EmployeeSalary(employee_id=employee.id, month=entry_date.month, year=entry_date.year, salary=0.0)
        emp_sal.salary += hours * employee_type.rate
        db.session.add(emp_sal)
        db.session.commit()
        # Enter data
        return redirect('/employees')
    return render_template('add_time.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        admin = Admin.query.filter_by(username=form.data['username']).first()
        if admin and admin.verify_password(form.data['password']):
            login_user(admin)
            g.user = current_user
            return redirect('/employees')
    return render_template('login.html', title='Sign In', form=form)


@app.route('/add_employee_type', methods=['GET', 'POST'])
@login_required
def add_employee_type():
    form = EmployeeTypeForm()
    if form.validate_on_submit():
        empt = EmployeeType.query.filter_by(slug=form.data['slug']).first()
        if not empt:
            empt = EmployeeType(slug=form.data['slug'], rate=form.data['rate'], description=form.data['description'])
        else:
            empt.rate = form.data['rate']
            empt.description = form.data['description']
        db.session.add(empt)
        db.session.commit()
        return redirect('/view_employee_type')
    return render_template('add_emp_type.html', form=form)


@app.route('/view_employee_type')
@login_required
def view_employee_type():
    emp_types = EmployeeType.query.all()
    return render_template('employee_types.html', emp_types=emp_types)


@app.route('/salary', methods=['GET', 'POST'])
@login_required
def salary():
    form = SalarySearchForm()
    employees = Employee.query
    if form.data.get('emp_type') != 'None':
        employees = employees.filter_by(employee_type_id=form.data['emp_type'])
    employees = employees.all()
    return render_template('salary.html', employees=employees, form=form)


@app.route('/delete/emp_hours/<object_id>', methods=['GET', 'POST'])
@login_required
def delete_object(object_id):
    form = EmployeeHourDeleteForm()
    emp_hour = EmployeeHours.query.get(object_id)
    employee = emp_hour.employee
    emp_type = [data for data in split_by_emp_type(employee.emp_id)][0]
    employee_type = EmployeeType.query.filter_by(slug=emp_type).first()
    emp_salary = EmployeeSalary.query.filter_by(employee_id=emp_hour.employee_id, month=emp_hour.entry_date.month, year=emp_hour.entry_date.year).first()
    amount = emp_hour.hours * employee_type.rate
    if form.validate_on_submit():
        emp_salary.salary -= amount
        db.session.delete(emp_hour)
        db.session.add(emp_salary)
        db.session.commit()
        flash('Employee hours deleted successfully')
        return redirect('/employees')
    form.object_id.data = emp_hour.id
    return render_template('employee_details.html', emp_hour=emp_hour, employee=employee, salary=amount, form=form)


@app.context_processor
def utility_processor():
    def get_hours_salary(hours, salaries, form):
        total_hours = 0.0
        total_salary = 0.0
        filter_key = None
        if form.data['month'] != 'None' and form.data['year'] != 'None':
            filter_key = (int(form.data['month']), int(form.data['year']))
        for hour in hours:
            if filter_key:
                entry_date = datetime.strptime(hour.entry_date, '%Y-%m-%d %H:%M:%S')
                if filter_key == (entry_date.month, entry_date.year):
                    total_hours += hour.hours
            else:
                total_hours += hour.hours
        for salary in salaries:
            if filter_key:
                if filter_key == (salary.month, salary.year):
                    total_salary = salary.salary
            else:
                total_salary += salary.salary
        return (total_hours, total_salary)
    return dict(get_hours_salary=get_hours_salary)
